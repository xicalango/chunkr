
pub mod metadata;
pub mod info;

pub(crate) mod utils;
mod sql_statements;

use std::collections::HashMap;
use std::fs::{File};
use std::io::{BufReader, BufWriter, Read, Write};
use std::path::Path;
use std::str::FromStr;

use hex;
use libflate::gzip::{Decoder, Encoder};
use rusqlite::{Connection, NO_PARAMS, params, Transaction};
use sha2::{Digest, Sha256};
use thiserror::Error;
pub use metadata::TreeMetadata;
use crate::info::{FileInfo, TreeVersionInfo, VersionInfo};

pub const CHUNK_SIZE: usize = 8192;

#[derive(Error, Debug)]
pub enum RepoError {
    #[error("io error: {0}")]
    IoError(#[from] std::io::Error),

    #[error("database error: {0}")]
    DbError(#[from] rusqlite::Error),

    #[error("unknown repo error")]
    Unknown,
}

pub type Result<T> = std::result::Result<T, RepoError>;

#[derive(Debug)]
pub struct Chunk(Vec<u8>);

pub type VersionId = i64;
pub type PathId = i64;

#[derive(Debug)]
pub enum VersionIdentifier {
    Number(i64),
    Alias(String),
}

impl FromStr for VersionIdentifier {
    type Err = RepoError;

    fn from_str(value: &str) -> Result<Self> {
        let number_value = value.parse::<i64>();
        match number_value {
            Ok(v) => Ok(VersionIdentifier::Number(v)),
            Err(_) => Ok(VersionIdentifier::Alias(value.to_owned())),
        }
    }
}

impl Chunk {
    fn decompress(compressed: &[u8]) -> Result<Chunk> {
        let mut gzip_decoder = Decoder::new(compressed)?;
        let mut buffer = Vec::new();
        gzip_decoder.read_to_end(&mut buffer)?;

        Ok(Chunk(buffer))
    }

    pub fn write_to<W: Write>(&self, writer: &mut W) -> Result<()> {
        writer.write_all(&self.0)?;
        Ok(())
    }
}

pub struct ChunkRef<'a>(&'a [u8]);

impl<'a> ChunkRef<'a> {
    pub fn compress(&self) -> Result<Vec<u8>> {
        let mut gzip_encoder = Encoder::new(Vec::new())?;
        gzip_encoder.write_all(&self.0)?;
        let compressed = gzip_encoder.finish().into_result()?;
        Ok(compressed)
    }

    pub fn hash(&self) -> String {
        hex::encode(Sha256::digest(&self.0))
    }
}

pub struct Repo {
    conn: Connection,
}

pub struct VersionBuilder<'a> {
    tx: Transaction<'a>,
    version_id: VersionId,
}

impl<'a> VersionBuilder<'a> {
    pub fn new(tx: Transaction<'a>, version_id: VersionId) -> VersionBuilder<'a> {
        VersionBuilder {
            tx,
            version_id,
        }
    }

    pub fn store_file_with_path<P: AsRef<Path>>(&mut self, path: P, store_path: &str) -> Result<String> {
        let path = path.as_ref();
        let metadata = path.metadata()?;
        let tree_metadata: TreeMetadata = metadata.into();
        let path_id = self.store_path(&store_path)?;
        let blob_hash = self.store_blob_from_path(path)?;
        self.store_tree_from_metadata(&self.version_id(), &path_id, &blob_hash, &tree_metadata)?;
        Ok(blob_hash)
    }

    pub fn store_file<P: AsRef<Path>>(&mut self, path: P) -> Result<String> {
        let path = path.as_ref();
        let path_str = path.to_str().ok_or_else(|| RepoError::Unknown)?; // TODO error

        self.store_file_with_path(path, path_str)
    }

    pub fn store_blob<R: Read>(&mut self, blob_source: R, store_path: &str, metadata: &TreeMetadata) -> Result<String> {
        let path_id = self.store_path(store_path)?;
        let blob_hash = self.store_blob_internal(blob_source)?;
        self.store_tree_from_metadata(&self.version_id(), &path_id, &blob_hash, metadata)?;

        Ok(blob_hash)
    }

    pub fn version_id(&self) -> VersionId {
        self.version_id
    }

    pub fn finish(self) -> Result<VersionId> {
        self.tx.commit()?;
        Ok(self.version_id)
    }

    fn store_blob_internal<R: Read>(&mut self, mut data: R) -> Result<String> {
        let mut chunk = [0; CHUNK_SIZE];
        let mut blob_hasher = Sha256::new();
        let mut chunk_hashes = Vec::new();
        {
            let mut stmt = self.tx.prepare(sql_statements::update::INSERT_CHUNK)?;

            loop {
                let n = data.read(&mut chunk)?;
                if n == 0 {
                    break;
                }

                let read_chunk = ChunkRef(&chunk[..n]);
                blob_hasher.input(read_chunk.0);

                let chunk_hash = read_chunk.hash();

                let exists: bool = self.tx.query_row(sql_statements::select::CHUNK_EXISTS, params![&chunk_hash], |row| row.get(0))?;

                if !exists {
                    let compressed = read_chunk.compress()?;
                    stmt.execute(params![&chunk_hash, &compressed])?;
                }

                chunk_hashes.push(chunk_hash);
            }
        }

        let blob_hash = hex::encode(blob_hasher.result());

        let exists: bool = self.tx.query_row(
            sql_statements::select::BLOB_EXISTS,
            params![&blob_hash],
            |row| row.get(0),
        )?;

        if !exists {
            self.tx.execute(sql_statements::update::INSERT_BLOB, params![&blob_hash])?;
            let blob_id = self.tx.last_insert_rowid();

            let mut stmt = self.tx.prepare(sql_statements::update::INSERT_BLOB_CHUNK)?;

            for (seq, chunk_hash) in chunk_hashes.iter().enumerate() {
                stmt.execute(params![&blob_id, chunk_hash, seq as i64])?;
            }
        }

        Ok(blob_hash)
    }

    fn store_blob_from_path<P: AsRef<Path>>(&mut self, path: P) -> Result<String> {
        let file = File::open(path)?;
        let buf_reader = BufReader::new(file);
        let blob_hash = self.store_blob_internal(buf_reader)?;

        Ok(blob_hash)
    }

    fn store_path(&mut self, path: &str) -> Result<PathId> {
        let mut stmt = self.tx.prepare(sql_statements::update::INSERT_PATH)?;
        stmt.execute(params![path])?;

        let mut stmt = self.tx.prepare(sql_statements::select::PATH_ID)?;
        let path_id: PathId = stmt.query_row(params![path], |row| row.get(0))?;

        Ok(path_id)
    }

    fn store_tree_from_metadata(&mut self, version_id: &VersionId, path_id: &PathId, blob_hash: &str, metadata: &TreeMetadata) -> Result<()> {
        let mut stmt = self.tx.prepare(sql_statements::update::INSERT_TREE)?;
        stmt.execute(params![
            version_id,
            path_id,
            blob_hash,
            &metadata.created_ts,
            &metadata.modified_ts,
            &metadata.size,
            &metadata.permissions,
            &metadata.file_type
        ])?;
        Ok(())
    }

}

pub struct VersionArchive<'a> {
    repo: &'a mut Repo,
    version_id: VersionId,
}

impl<'a> VersionArchive<'a> {
    pub fn new(repo: &'a mut Repo, version_id: VersionId) -> VersionArchive<'a> {
        VersionArchive {
            repo,
            version_id,
        }
    }

    pub fn write_file_by_path<P: AsRef<Path>>(&self, path: &str, output_path: P) -> Result<String> {
        let blob_hash = self.repo.get_blob_hash(&self.version_id, path)?;
        self.repo.write_file_by_hash(&blob_hash, &output_path)?;
        let metadata = self.repo.get_tree_metadata(&self.version_id, path)?;
        metadata.apply(&output_path)?;
        Ok(blob_hash)
    }

    pub fn write_file(&self, path: &str) -> Result<String> {
        self.write_file_by_path(path, path)
    }

    pub fn list_files(&self) -> Result<Vec<String>> {
        self.repo.list_files(&self.version_id)
    }

    pub fn list_files_with_hash(&self) -> Result<Vec<FileInfo>> {
        self.repo.list_files_with_hash(&self.version_id)
    }

    pub fn get_metadata(&self) -> Result<HashMap<String, String>> {
        self.repo.get_version_metadata(&self.version_id)
    }

    pub fn version_id(&self) -> VersionId {
        self.version_id
    }

    pub fn update_alias(&self, alias: &str) -> Result<()> {
        self.repo.update_version_alias(&self.version_id, alias)
    }

    pub fn put_all_metadata(&mut self, metadata: &HashMap<String, String>) -> Result<()> {
        self.repo.update_version_metadata(&self.version_id, metadata)
    }

    pub fn delete_metadata(&mut self, key: &str) -> Result<()> {
        self.repo.delete_version_metadata(&self.version_id, key)
    }

    pub fn get_alias(&self) -> Result<Option<String>> {
        self.repo.get_version_alias(&self.version_id)
    }
}

impl Repo {
    pub fn open<R: AsRef<Path>>(path: R) -> Result<Repo> {
        let repo_path = path.as_ref().to_owned();
        let conn = Connection::open(&repo_path)?;

        Ok(Repo { conn })
    }
    pub fn init(&mut self) -> Result<()> {
        use sql_statements::init::*;
        let tx = self.conn.transaction()?;
        tx.execute(TABLE_CHUNK, NO_PARAMS)?;
        tx.execute(TABLE_BLOB, NO_PARAMS)?;
        tx.execute(TABLE_BLOB_CHUNK, NO_PARAMS)?;
        tx.execute(TABLE_TREE, NO_PARAMS)?;
        tx.execute(TABLE_PATHS, NO_PARAMS)?;
        tx.execute(TABLE_VERSION, NO_PARAMS)?;
        tx.execute(TABLE_VERSION_METADATA, NO_PARAMS)?;
        tx.execute(VIEW_DATA, NO_PARAMS)?;
        tx.execute(VIEW_PATHS, NO_PARAMS)?;
        tx.commit()?;
        Ok(())
    }

    pub fn assert_initialized(&self) -> Result<()> {
        // TODO @aweld check for sth
        Ok(())
    }

    fn get_blob_hash(&self, version_id: &VersionId, path: &str) -> Result<String> {
        let mut stmt = self.conn.prepare(sql_statements::select::HASH_FOR_VERSION)?;
        let blob_hash: String = stmt.query_row(params![version_id, path], |f| f.get(0))?;

        Ok(blob_hash)
    }

    pub fn write_file_by_hash<P: AsRef<Path>>(&self, hash: &str, path: P) -> Result<()> {
        let path = path.as_ref();
        if let Some(parent_path) = path.parent() {
            std::fs::create_dir_all(parent_path)?;
        }
        let file = File::create(path)?;
        let mut buf_write = BufWriter::new(file);
        self.write_all(&hash, &mut buf_write)?;
        Ok(())
    }

    pub fn update_version_alias(&self, version_id: &VersionId, alias: &str) -> Result<()> {
        let mut stmt = self.conn.prepare(sql_statements::update::UPDATE_ALIAS)?;
        stmt.execute(params![alias, version_id])?;
        Ok(())
    }

    pub fn update_version_metadata(&mut self, version_id: &VersionId, metadata: &HashMap<String, String>) -> Result<()> {
        let tx = self.conn.transaction()?;
        {
            let mut metadata_stmt = tx.prepare(sql_statements::update::UPSERT_VERSION_METADATA)?;
            for (key, value) in metadata {
                metadata_stmt.execute(params![version_id, key, value])?;
            }
        }
        tx.commit()?;
        Ok(())
    }

    pub fn delete_version_metadata(&mut self, version_id: &VersionId, key: &str) -> Result<()> {
        let mut stmt = self.conn.prepare(sql_statements::update::REMOVE_VERSION_METADATA)?;
        stmt.execute(params![version_id, key])?;
        Ok(())
    }

    pub fn get_version_alias(&self, version_id: &VersionId) -> Result<Option<String>> {
        let alias: Option<String> = self.conn.query_row(sql_statements::select::VERSION_ALIAS,
                                                   params![version_id],
                                                   |r| r.get(0))?;

        Ok(alias)
    }

    pub fn get_tree_metadata(&self, version_id: &VersionId, path: &str) -> Result<TreeMetadata> {
        let metadata = self.conn.query_row(sql_statements::select::TREE_METADATA, params![version_id, path],
        |r| Ok(TreeMetadata {
            created_ts: r.get(0)?,
            modified_ts: r.get(1)?,
            size: r.get(2)?,
            permissions: r.get(3)?,
            file_type: r.get(4)?,
        }))?;

        Ok(metadata)
    }

    pub fn get_all_hashes_for_tree(&self, path: &str) -> Result<Vec<TreeVersionInfo>> {
        let mut stmt = self.conn.prepare(sql_statements::select::HASHES_OF_TREE_DISTINCT)?;

        let results = stmt.query_map(params![path], |row| Ok(TreeVersionInfo {
            version_info: None,
            file_info: FileInfo {
                path: path.to_string(),
                hash: row.get(0)?,
            },
            metadata: TreeMetadata {
                created_ts: row.get(1)?,
                modified_ts: row.get(2)?,
                size: row.get(3)?,
                permissions: None,
                file_type: None,
            }
        }))?;

        let results: Vec<TreeVersionInfo> = results.filter_map(|r| r.ok()).collect();
        Ok(results)
    }

    pub fn get_version_info_for_tree(&self, path: &str) -> Result<Vec<TreeVersionInfo>> {
        let mut stmt = self.conn.prepare(sql_statements::select::VERSIONS_OF_TREE)?;

        let results = stmt.query_map(params![path], |row| Ok(TreeVersionInfo {
            version_info: Some(VersionInfo {
                id: row.get(0)?,
                timestamp: row.get(1)?,
                alias: row.get(2)?,
                metadata: None,
            }),
            file_info: FileInfo {
                path: path.to_string(),
                hash: row.get(3)?,
            },
            metadata: TreeMetadata {
                created_ts: row.get(4)?,
                modified_ts: row.get(5)?,
                size: row.get(6)?,
                permissions: row.get(7)?,
                file_type: row.get(8)?,
            }
        }))?;

        let results: Vec<TreeVersionInfo> = results.filter_map(|r| r.ok()).collect();
        Ok(results)
    }


    pub fn get_version_info_for_hash(&self, hash: &str) -> Result<Vec<TreeVersionInfo>> {
        let mut stmt = self.conn.prepare(sql_statements::select::VERSIONS_OF_HASH)?;

        let results = stmt.query_map(params![hash], |row| Ok(TreeVersionInfo {
            version_info: Some(VersionInfo {
                id: row.get(0)?,
                timestamp: row.get(1)?,
                alias: row.get(2)?,
                metadata: None,
            }),
            file_info: FileInfo {
                path: row.get(3)?,
                hash: hash.to_string(),
            },
            metadata: TreeMetadata {
                created_ts: row.get(4)?,
                modified_ts: row.get(5)?,
                size: row.get(6)?,
                permissions: row.get(7)?,
                file_type: row.get(8)?,
            }
        }))?;

        let results: Vec<TreeVersionInfo> = results.filter_map(|r| r.ok()).collect();
        Ok(results)
    }

    pub fn add_version(&mut self, alias: Option<&str>, metadata: Option<&HashMap<String, String>>) -> Result<VersionBuilder<'_>> {
        let tx = self.conn.transaction()?;
        let version_id: i64;

        {
            let mut stmt = tx.prepare(sql_statements::update::INSERT_VERSION)?;
            stmt.execute(params![alias])?;
            version_id = tx.last_insert_rowid();

            if let Some(metadata) = metadata {
                let mut metadata_stmt = tx.prepare(sql_statements::update::INSERT_VERSION_METADATA)?;
                for (key, value) in metadata {
                    metadata_stmt.execute(params![version_id, key, value])?;
                }
            }
        }

        Ok(VersionBuilder::new(tx, version_id))
    }

    pub fn extract(&mut self, version_id: &VersionId) -> VersionArchive<'_> {
        VersionArchive::new(self, version_id.clone())
    }

    pub fn extract_by_identifier(&mut self, version_id: &VersionIdentifier) -> Result<VersionArchive<'_>> {
        let version_id = self.get_version_id(version_id)?;
        Ok(self.extract(&version_id))
    }

    pub fn get_version_id_from_alias(&self, alias: &str) -> Result<VersionId> {
        let id: VersionId = self.conn.query_row(sql_statements::select::VERSION_ID_FROM_ALIAS, params![alias], |row| row.get(0))?;
        Ok(id)
    }

    pub fn list_files(&self, version_id: &VersionId) -> Result<Vec<String>> {
        let mut stmt = self.conn.prepare(sql_statements::select::PATHS_FOR_VERSION)?;
        let rows = stmt.query_map(params![version_id], |row| row.get(0))?;

        let rows: Vec<String> = rows.filter_map(|r| r.ok()).collect(); // TODO propagate errors
        Ok(rows)
    }

    pub fn list_files_with_hash(&self, version_id: &VersionId) -> Result<Vec<FileInfo>> {
        let mut stmt = self.conn.prepare(sql_statements::select::PATHS_FOR_VERSION)?;
        let rows = stmt.query_map(params![version_id], |row| Ok(FileInfo{
            path: row.get(0)?,
            hash: row.get(1)?,
        }))?;

        let rows: Vec<FileInfo> = rows.filter_map(|r| r.ok()).collect(); // TODO propagate errors
        Ok(rows)
    }

    pub fn get_version_metadata(&self, version_id: &VersionId) -> Result<HashMap<String, String>>{
        let mut stmt = self.conn.prepare(sql_statements::select::VERSION_METADATA)?;
        let rows = stmt.query_map(params![version_id], |row| Ok((row.get(0)?, row.get(1)?)))?;

        let metadata: HashMap<String, String> = rows.filter_map(|r| r.ok()).collect();
        Ok(metadata)
    }

    pub fn get_versions(&self) -> Result<Vec<VersionInfo>> {
        let mut stmt = self.conn.prepare(sql_statements::select::VERSIONS)?;
        let rows = stmt.query_map(params![], |row| Ok(VersionInfo {
            id: row.get(0)?,
            timestamp: row.get(1)?,
            alias: row.get(2)?,
            metadata: None,
        }))?;

        let rows: std::result::Result<Vec<VersionInfo>, rusqlite::Error> = rows.collect();

        Ok(rows?)
    }

    pub fn write_all<W: Write>(&self, blob_hash: &str, writer: &mut W) -> Result<()> {
        let mut stmt = self.conn.prepare(sql_statements::select::DATA_FOR_BLOB)?;
        let rows = stmt.query_map(params![&blob_hash], |row| row.get(0))?;

        for row in rows {
            let row: Vec<u8> = row?;
            let chunk = Chunk::decompress(&row)?;
            chunk.write_to(writer)?;
        }

        Ok(())
    }

    pub fn get_version_id(&self, version_identifier: &VersionIdentifier) -> Result<VersionId> {
        match &version_identifier {
            &VersionIdentifier::Number(id) => Ok(*id),
            &VersionIdentifier::Alias(alias) => Ok(self.get_version_id_from_alias(&alias)?),
        }
    }
}

#[cfg(test)]
mod test {
    use std::fs::{read_to_string, remove_file, write};
    use std::io::Cursor;

    use super::*;

    static TEST_DB_NAME: &str = "test.chunkr.db";
    static TEST_FILE_NAME: &str = "test.file";
    pub static TEST_DATA: &str = "hello_world";

    pub struct TestRepo {
        pub db_path: String,
        pub test_file_name: String,
    }

    impl TestRepo {
        pub fn new() -> TestRepo {
            let uniquifier = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_nanos();
            let db_path = format!("{}_{}", uniquifier, TEST_DB_NAME);
            let test_file_name = format!("{}_{}", uniquifier, TEST_FILE_NAME);

            write(&test_file_name, TEST_DATA).unwrap();

            TestRepo {
                db_path,
                test_file_name,
            }
        }

        pub fn repo(&self) -> Repo {
            let mut repo = Repo::open(&self.db_path).unwrap();
            repo.init().unwrap();
            repo
        }

        pub fn test_file_name(&self) -> &str {
            &self.test_file_name
        }
    }

    impl Drop for TestRepo {
        fn drop(&mut self) {
            remove_file(&self.db_path).unwrap();
            remove_file(&self.test_file_name).unwrap();
        }
    }

    #[test]
    fn test_init() {
        let test_repo = TestRepo::new();
        test_repo.repo();
    }

    #[test]
    fn test_write() {
        let test_repo = TestRepo::new();
        let mut repo = test_repo.repo();

        let data = b"test";
        let cursor = Cursor::new(data);
        let mut vb = repo.add_version(None, None).unwrap();
        vb.store_blob_internal(cursor).unwrap();
        vb.finish().unwrap();
    }

    #[test]
    fn test_write_big() {
        let test_repo = TestRepo::new();
        let mut repo = test_repo.repo();

        let data = vec![0; 32768];
        let cursor = Cursor::new(data);
        let mut vb = repo.add_version(None, None).unwrap();
        vb.store_blob_internal(cursor).unwrap();
        vb.finish().unwrap();
    }

    #[test]
    fn test_read_write_write() {
        let test_repo = TestRepo::new();
        let mut repo = test_repo.repo();

        let data = vec![0; 32768];
        let cursor = Cursor::new(&data);
        let mut vb = repo.add_version(None, None).unwrap();
        let hash = vb.store_blob_internal(cursor).unwrap();
        vb.finish().unwrap();

        let mut read_data: Vec<u8> = Vec::new();

        repo.write_all(&hash, &mut read_data).unwrap();

        assert_eq!(&data, &read_data);
    }

    #[test]
    fn test_version() {
        let test_repo = TestRepo::new();
        let mut repo = test_repo.repo();
        repo.add_version(None, None).unwrap();
    }

    #[test]
    fn test_add_get_versions() {
        let test_repo = TestRepo::new();
        let mut repo = test_repo.repo();
        let vb1 = repo.add_version(Some("test"), None).unwrap();
        let v1 = vb1.finish().unwrap();
        let vb2 = repo.add_version(Some("other"), None).unwrap();
        let v2 = vb2.finish().unwrap();

        assert_ne!(v1, v2);

        let versions = repo.get_versions().unwrap();

        assert!(versions.iter().find(|i| i.id == v1).is_some());
        assert!(versions.iter().find(|i| i.alias == Some("test".to_string())).is_some());
        assert!(versions.iter().find(|i| i.id == v2).is_some());
        assert!(versions.iter().find(|i| i.alias == Some("other".to_string())).is_some());
        assert_eq!(versions.len(), 2);

        let v_from_alias1 = repo.get_version_id_from_alias("test").unwrap();
        assert_eq!(v_from_alias1, v1);
        let v_from_alias2 = repo.get_version_id_from_alias("other").unwrap();
        assert_eq!(v_from_alias2, v2);
    }

    #[test]
    fn test_add_file() {
        let test_repo = TestRepo::new();
        let mut repo = test_repo.repo();

        let mut version_builder = repo.add_version(None, None).unwrap();
        let file_hash = version_builder.store_file(test_repo.test_file_name()).unwrap();
        version_builder.finish().unwrap();

        let mut read_data: Vec<u8> = Vec::new();
        repo.write_all(&file_hash, &mut read_data).unwrap();
        assert_eq!(TEST_DATA.as_bytes(), &read_data[..]);
    }

    #[test]
    fn test_add_and_write_file() {
        let test_repo = TestRepo::new();
        let mut repo = test_repo.repo();

        let mut version_builder = repo.add_version(None, None).unwrap();
        version_builder.store_file(test_repo.test_file_name()).unwrap();
        let version_id = version_builder.finish().unwrap();

        let write_file_name = format!("{}.output", test_repo.test_file_name());

        let archive = repo.extract(&version_id);

        archive.write_file_by_path(test_repo.test_file_name(), &write_file_name).unwrap();
        let read_data = read_to_string(&write_file_name).unwrap();
        remove_file(&write_file_name).unwrap();

        assert_eq!(TEST_DATA, &read_data);
    }
}

#[cfg(all(test, unix))]
mod unix_test {

    use super::*;
    use super::test::{TestRepo, TEST_DATA};
    use std::fs::{read_to_string, remove_file, Permissions, set_permissions};
    use std::os::unix::fs::PermissionsExt;

    #[test]
    fn test_read_write_file_with_permission() {
        let test_repo = TestRepo::new();
        let mut repo = test_repo.repo();

        let path: &Path = test_repo.test_file_name().as_ref();
        let permissions = Permissions::from_mode(0o675);

        set_permissions(&path, permissions.clone()).unwrap();

        let mut version_builder = repo.add_version(None, None).unwrap();
        version_builder.store_file(test_repo.test_file_name()).unwrap();
        let version_id = version_builder.finish().unwrap();

        let write_file_name = format!("{}.output", test_repo.test_file_name());

        let archive = repo.extract(&version_id);

        archive.write_file_by_path(test_repo.test_file_name(), &write_file_name).unwrap();
        let read_data = read_to_string(&write_file_name).unwrap();
        let out_path: &Path = write_file_name.as_ref();
        let out_permissions = out_path.metadata().unwrap().permissions();
        remove_file(&write_file_name).unwrap();

        let mode_orig = permissions.mode() & 0o777;
        let mode_out = out_permissions.mode() & 0o777;
        assert_eq!(mode_orig, mode_out);
        assert_eq!(TEST_DATA, &read_data);
    }

}
