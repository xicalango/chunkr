
use crate::{VersionId, TreeMetadata};
use std::collections::HashMap;
use std::fmt::{Display, Formatter};


#[derive(Debug)]
pub struct VersionInfo {
    pub id: VersionId,
    pub timestamp: i64,
    pub alias: Option<String>,
    pub metadata: Option<HashMap<String, String>>,
}

impl Display for VersionInfo {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.id)?;
        if let Some(alias) = self.alias.as_ref() {
            write!(f, ":{}", alias)?;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct FileInfo {
    pub path: String,
    pub hash: String,
}

impl Display for FileInfo {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.path, self.hash)
    }
}

#[derive(Debug)]
pub struct TreeVersionInfo {
    pub version_info: Option<VersionInfo>,
    pub file_info: FileInfo,
    pub metadata: TreeMetadata,
}
