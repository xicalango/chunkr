
pub mod init {

    pub static TABLE_CHUNK: &str = 
    "CREATE TABLE IF NOT EXISTS chunk(
        hash TEXT PRIMARY KEY,
        data BLOB
    );";

    pub static TABLE_BLOB_CHUNK: &str = 
    "CREATE TABLE IF NOT EXISTS blob_chunk(
        blob_id INTEGER,
        chunk_hash TEXT,
        seq INTEGER
    );";

    pub static TABLE_BLOB: &str = 
    "CREATE TABLE IF NOT EXISTS blob(
        id INTEGER PRIMARY KEY,
        hash TEXT UNIQUE
    );";

    pub static TABLE_TREE: &str =
    "CREATE TABLE IF NOT EXISTS tree(
        version_id INTEGER,
        path_id INTEGER,
        blob_hash TEXT,
        created_ts INTEGER,
        modified_ts INTEGER,
        size INTEGER,
        permissions INTEGER,
        file_type INTEGER,
        UNIQUE(version_id, path_id)
    );";

    pub static TABLE_VERSION: &str =
    "CREATE TABLE IF NOT EXISTS version(
        id INTEGER PRIMARY KEY,
        timestamp INTEGER,
        alias TEXT UNIQUE
    );";

    pub static TABLE_VERSION_METADATA: &str =
    "CREATE TABLE IF NOT EXISTS version_metadata(
        version_id INTEGER,
        key TEXT,
        value TEXT,
        PRIMARY KEY(version_id, key)
    );";

    pub static TABLE_PATHS: &str =
        "CREATE TABLE IF NOT EXISTS path(
            id INTEGER PRIMARY KEY,
            path TEXT UNIQUE
    );";

    pub static VIEW_DATA: &str =
    "CREATE VIEW IF NOT EXISTS blob_data(
        hash,
        seq,
        data
    ) AS
    SELECT blob.hash, blob_chunk.seq, chunk.data 
    FROM blob, blob_chunk, chunk
    WHERE blob.id = blob_chunk.blob_id
        AND blob_chunk.chunk_hash = chunk.hash
    ;";

    pub static VIEW_PATHS: &str =
    "CREATE VIEW IF NOT EXISTS version_path_hash(
        version_id,
        blob_hash,
        path
    ) AS
    SELECT tree.version_id, tree.blob_hash, path.path
    FROM tree, path
    WHERE tree.path_id = path.id
    ;";
}

pub mod update {

    pub static INSERT_CHUNK: &str =
    "INSERT OR IGNORE INTO chunk(
        hash, 
        data
    ) VALUES (
        ?,
        ?
    );";

    pub static INSERT_BLOB: &str =
    "INSERT INTO blob(
        hash
    ) VALUES (
        ?
    );";

    pub static INSERT_BLOB_CHUNK: &str =
    "INSERT INTO blob_chunk(
        blob_id, 
        chunk_hash,
        seq
    ) VALUES (
        ?,
        ?,
        ?
    );";

    pub static INSERT_VERSION: &str =
    "INSERT INTO version(
        timestamp,
        alias
    ) VALUES (
        strftime('%s', 'now'),
        ?
    );";

    pub static INSERT_VERSION_METADATA: &str =
        "INSERT INTO version_metadata(
        version_id,
        key,
        value
    ) VALUES (
        ?,
        ?,
        ?
    );";

    pub static INSERT_TREE: &str =
    "INSERT INTO tree(
        version_id,
        path_id,
        blob_hash,
        created_ts,
        modified_ts,
        size,
        permissions,
        file_type
    ) VALUES (
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?
    );";

    pub static INSERT_PATH: &str =
    "INSERT OR IGNORE INTO path(
        path
    ) VALUES (
        ?
    );";

    pub static UPDATE_ALIAS: &str = "UPDATE version SET alias = ? WHERE id = ?;";

    pub static UPSERT_VERSION_METADATA: &str =
        "REPLACE INTO version_metadata(
        version_id,
        key,
        value
    ) VALUES (
        ?,
        ?,
        ?
    );";

    pub static REMOVE_VERSION_METADATA: &str =
    "DELETE FROM version_metadata WHERE version_id = ? AND key = ?;";

}

pub mod select {
    pub static CHUNK_EXISTS: &str = "SELECT EXISTS(SELECT 1 FROM chunk WHERE hash = ?);";
    pub static BLOB_EXISTS: &str = "SELECT EXISTS(SELECT 1 FROM blob WHERE hash = ?);";
    pub static DATA_FOR_BLOB: &str = "SELECT data FROM blob_data WHERE hash = ? ORDER BY seq ASC;";
    pub static PATH_ID: &str = "SELECT id FROM path WHERE path = ?;";
    pub static HASH_FOR_VERSION: &str = "SELECT blob_hash FROM version_path_hash WHERE version_id = ? AND path = ?;";
    pub static PATHS_FOR_VERSION: &str = "SELECT path, blob_hash FROM version_path_hash WHERE version_id = ?;";
    pub static VERSION_METADATA: &str = "SELECT key, value FROM version_metadata WHERE version_id = ?;";
    pub static VERSION_ID_FROM_ALIAS: &str = "SELECT id FROM version WHERE alias = ?;";
    pub static VERSIONS: &str = "SELECT id, timestamp, alias FROM version ORDER BY id DESC;";
    pub static VERSION_ALIAS: &str = "SELECT alias FROM version WHERE id = ?;";

    pub static TREE_METADATA: &str =
        "SELECT t.created_ts, t.modified_ts, t.size, t.permissions, t.file_type
        FROM tree t, path p
        WHERE t.path_id = p.id AND p.path = ?2 AND t.version_id = ?1;";

    pub static VERSIONS_OF_TREE: &str =
        "SELECT version_id, version.timestamp, version.alias, blob_hash, created_ts, modified_ts, size, permissions, file_type
        FROM tree, path, version
        WHERE tree.path_id = path.id AND version.id = tree.version_id AND path.path = ?1
        ORDER BY version_id DESC;";

    pub static VERSIONS_OF_HASH: &str =
        "SELECT version_id, version.timestamp, version.alias, path.path, created_ts, modified_ts, size, permissions, file_type
        FROM tree, path, version
        WHERE tree.path_id = path.id AND version.id = tree.version_id AND tree.blob_hash = ?1
        ORDER BY version_id DESC;";

    pub static HASHES_OF_TREE_DISTINCT: &str =
        "SELECT blob_hash, max(created_ts), max(modified_ts), max(size)
        FROM tree, path
        WHERE tree.path_id = path.id AND path.path = ?1
        GROUP BY blob_hash
        ORDER BY max(modified_ts) DESC;";
}
