use std::time::SystemTime;

pub fn convert_ts(time: Option<SystemTime>) -> Option<i64> {
    time.and_then(|t| t.duration_since(SystemTime::UNIX_EPOCH).ok())
        .map(|d| d.as_millis() as i64)
}
