
use std::fs::Metadata;
use std::fs::Permissions;

use crate::utils;
use std::path::Path;

use crate::Result;

#[derive(Debug, Default)]
pub struct TreeMetadata {
    pub created_ts: Option<i64>,
    pub modified_ts: Option<i64>,
    pub size: i64,
    pub permissions: Option<u32>,
    pub file_type: Option<i64>,
}

#[cfg(unix)]
fn get_permissions(metadata: &Metadata) -> Option<u32> {
    use std::os::unix::fs::PermissionsExt;
    let permissions = metadata.permissions();
    Some(permissions.mode())
}

#[cfg(not(unix))]
fn get_permissions(metadata: &Metadata) -> Option<u32> {
    None
}

impl From<Metadata> for TreeMetadata {
    fn from(metadata: Metadata) -> Self {
        let modified_ts = utils::convert_ts(metadata.modified().ok());
        let created_ts = utils::convert_ts(metadata.created().ok());
        let size = metadata.len() as i64;

        let permissions = get_permissions(&metadata);

        TreeMetadata {
            created_ts,
            modified_ts,
            size,
            permissions,
            file_type: None,
        }
    }
}

impl TreeMetadata {

    pub fn apply<P: AsRef<Path>>(&self, path: P) -> Result<()> {
        if let Some(permissions) = self.permissions {
            self.apply_permissions(path.as_ref(), permissions)?;
        }
        Ok(())
    }

    #[cfg(unix)]
    fn apply_permissions(&self, path: &Path, permissions: u32) -> Result<()> {
        use std::os::unix::fs::PermissionsExt;
        let permissions = Permissions::from_mode(permissions);
        std::fs::set_permissions(path, permissions)?;
        Ok(())
    }

    #[cfg(not(unix))]
    fn apply_permissions(&self, path: &Path, permissions: u32) -> Result<()> {
        Ok(())
    }

}

#[cfg(all(unix,test))]
mod unix_test {

    use super::*;
    use std::fs::{File, Permissions};
    use std::path::PathBuf;
    use std::os::unix::fs::PermissionsExt;

    #[derive(Debug)]
    struct TestFile(PathBuf);

    impl TestFile {

        fn new() -> TestFile {
            TestFile::new_with_decoration("tmp_", "")
        }

        fn new_with_decoration(prefix: &str, suffix: &str) -> TestFile {
            let uniquifier = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_nanos();
            let name = format!("{}{}{}", prefix, uniquifier, suffix);

            let path: PathBuf = name.into();
            {
                File::create(&path).unwrap();
            }

            TestFile(path)
        }

        fn path(&self) -> &Path {
            &self.0
        }
    }

    impl Drop for TestFile {
        fn drop(&mut self) {
            std::fs::remove_file(&self.0).unwrap();
        }
    }

    #[test]
    fn test_get_permissions() {
        let file = TestFile::new();
        let metadata = file.path().metadata().unwrap();
        let mode = metadata.permissions().mode();

        let tree_metadata: TreeMetadata = metadata.into();
        assert_eq!(Some(mode), tree_metadata.permissions);
    }

    #[test]
    fn test_get_updated_permissions() {
        let file = TestFile::new();
        let new_permissions = Permissions::from_mode(0o600);
        std::fs::set_permissions(file.path(), new_permissions).unwrap();
        let metadata = file.path().metadata().unwrap();
        let mode = metadata.permissions().mode();
        assert_eq!(0o600, mode & 0o777);

        let tree_metadata: TreeMetadata = file.path().metadata().unwrap().into();
        assert_eq!(Some(mode), tree_metadata.permissions);
    }

    #[test]
    fn test_update_permissions() {
        let file = TestFile::new();

        let tree_metadata = TreeMetadata {
            permissions: Some(0o777),
            ..TreeMetadata::default()
        };

        tree_metadata.apply(file.path()).unwrap();

        let metadata = file.path().metadata().unwrap();
        let mode = metadata.permissions().mode();
        assert_eq!(0o777, mode & 0o777);
    }

}
