
use structopt::StructOpt;
use std::path::{PathBuf, Path};
use std::str::FromStr;

use anyhow::{Result, Error, anyhow};
use std::ffi::OsStr;
use std::fs::File;
use zip::ZipArchive;
use chunkr::{Repo, TreeMetadata, VersionBuilder};
use std::collections::HashMap;
use tar::{Archive, Entry};

#[derive(Debug, Copy, Clone)]
pub enum SourceType {
    Tar,
    Zip,
}

impl FromStr for SourceType {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        match s {
            "zip" => Ok(SourceType::Zip),
            "tar" => Ok(SourceType::Tar),
            other => Err(anyhow!("unknown filetype: {}", other)),
        }
    }
}

/// Program to import archives into a chunkr repo
#[derive(Debug, StructOpt)]
pub struct Opts {
    /// Alias of the added version
    #[structopt(short, long)]
    alias: Option<String>,

    /// If set, automatically create the alias from the file
    #[structopt(short("A"), long)]
    auto_alias: bool,

    /// Type of the source file. if not given it is guessed from the extension
    #[structopt(short, long)]
    file_type: Option<SourceType>,

    /// Source archive
    #[structopt(parse(from_os_str))]
    source_path: PathBuf,
}

fn import_zip_entry(version_builder: &mut VersionBuilder<'_>, archive: &mut ZipArchive<File>, i: usize) -> Result<Option<(String, String)>> {
    let file = archive.by_index(i)?;
    let outpath = match file.enclosed_name() {
        Some(path) => path.to_owned(),
        None => return Err(anyhow!("filename error")),
    };

    let outpath = outpath.to_str().unwrap();

    if file.name().ends_with('/') || file.is_dir() {
        return Ok(None);
    }

    let permissions = file.unix_mode();

    let tree_metadata = TreeMetadata {
        size: file.size() as i64,
        permissions,
        ..TreeMetadata::default()
    };

    let blob_hash = version_builder.store_blob(file, outpath, &tree_metadata)?;
    Ok(Some((outpath.to_string(), blob_hash)))
}

pub fn import_from_zip(repo_path: &PathBuf, verbose: bool, opts: &Opts) -> Result<()> {

    let mut repo = Repo::open(repo_path)?;
    repo.init()?;

    let source_file_name = if opts.auto_alias {
        opts.source_path.file_name().and_then(OsStr::to_str)
    } else {
        None
    };

    let alias = opts.alias.as_ref() //
        .map(|s| s.as_str()) //
        .or(source_file_name);

    let mut metadata = HashMap::new();

    let full_source_path = opts.source_path.to_str().unwrap();
    metadata.insert("imported_from".to_string(), full_source_path.to_string());

    let mut version_builder = repo.add_version(alias, Some(&metadata))?;

    let zip_file = File::open(&opts.source_path)?;
    let mut archive = ZipArchive::new(zip_file)?;

    let mut error = false;

    for i in 0..archive.len() {
        match import_zip_entry(&mut version_builder, &mut archive, i) {
            Ok(Some((outpath, blob_hash))) => {
                if verbose {
                    println!("{} {}", outpath, blob_hash);
                }
            },

            Ok(None) => {},

            Err(e) => {
                error = true;
                eprintln!("Error during processing: {:?}", e);
            }

        };
    }

    let version_id = version_builder.finish()?;

    if verbose {
        println!("Created version {}", version_id);
    }

    if error {
        Err(anyhow!("there were errors when storing the files"))
    } else {
        Ok(())
    }
}

fn import_tar_entry(version_builder: &mut VersionBuilder, file: std::io::Result<Entry<File>>) -> Result<Option<(String, String)>> {
    // Make sure there wasn't an I/O error
    let file = file?;

    let file_path = file.header().path()?.to_str().unwrap().to_owned();
    let file_size = file.header().size()? as i64;
    let file_mode = file.header().mode()?;

    if file_path.ends_with('/') {
        return Ok(None);
    }

    let tree_metadata = TreeMetadata {
        size: file_size,
        permissions: Some(file_mode),
        ..TreeMetadata::default()
    };



    let blob_hash = version_builder.store_blob(file, &file_path, &tree_metadata)?;

    Ok(Some((file_path, blob_hash)))
}

pub fn import_from_tar(repo_path: &PathBuf, verbose: bool, opts: &Opts) -> Result<()> {
    let mut repo = Repo::open(repo_path)?;
    repo.init()?;

    let source_file_name = if opts.auto_alias {
        opts.source_path.file_name().and_then(OsStr::to_str)
    } else {
        None
    };

    let mut metadata = HashMap::new();

    let full_source_path = opts.source_path.to_str().unwrap();
    metadata.insert("imported_from".to_string(), full_source_path.to_string());

    let mut version_builder = repo.add_version(source_file_name, Some(&metadata))?;

    let tar_file = File::open(&opts.source_path)?;
    let mut tar_archive = Archive::new(tar_file);

    let mut error = false;

    for file in tar_archive.entries()? {
        match import_tar_entry(&mut version_builder, file) {
            Ok(Some((outpath, blob_hash))) => {
                if verbose {
                    println!("{} {}", outpath, blob_hash);
                }
            },

            Ok(None) => {},

            Err(e) => {
                error = true;
                eprintln!("Error during processing: {:?}", e);
            }
        }
    }

    let version_id = version_builder.finish()?;

    if verbose {
        println!("Created version {}", version_id);
    }

    if error {
        Err(anyhow!("there were errors when storing the files"))
    } else {
        Ok(())
    }
}

pub fn import(repo_path: &PathBuf, opts: Opts, verbose: bool) -> Result<()> {
    let file_type = match opts.file_type.as_ref() {
        None => {
            Path::new(&opts.source_path)
                .extension()
                .and_then(OsStr::to_str)
                .unwrap()
                .parse()
                .unwrap()
        },
        Some(t) => *t
    };

    match file_type {
        SourceType::Zip => import_from_zip(repo_path, verbose, &opts),
        SourceType::Tar => import_from_tar(repo_path, verbose, &opts),
    }
}
