
use std::collections::HashMap;
use std::fs::File;
use std::str::FromStr;
use std::path::PathBuf;
use structopt::StructOpt;
use std::io::{BufRead, BufReader, BufWriter, Write};
use chrono::naive::NaiveDateTime;

use anyhow::{Result, Error, anyhow};

use chunkr::{Repo, VersionIdentifier, VersionBuilder};
use chunkr::info::TreeVersionInfo;

mod import;

mod util {
    use std::path::Path;
    use std::fs::DirEntry;
    use std::fs;
    use anyhow::Result;

    // one possible implementation of walking a directory only visiting files
    pub fn visit_dirs(dir: &Path, cb: &mut dyn FnMut(&DirEntry) -> Result<()>) -> Result<()> {
        if dir.is_dir() {
            for entry in fs::read_dir(dir)? {
                let entry = entry?;
                let path = entry.path();
                if path.is_dir() {
                    visit_dirs(&path, cb)?;
                } else {
                    cb(&entry)?;
                }
            }
        }
        Ok(())
    }
}

#[derive(Debug)]
struct MetadataKv {
    key: String,
    value: String,
}

#[derive(Debug)]
enum MetadataDescriptor {
    Kv(MetadataKv),
    File(PathBuf),
}

impl MetadataDescriptor {

    pub fn to_vec(self) -> Result<Vec<MetadataKv>> {
        match self {
            MetadataDescriptor::Kv(kv) => {
                Ok(vec![kv])
            }
            MetadataDescriptor::File(path) => {
                let mut kvs = Vec::new();
                let file = File::open(path)?;
                let reader = BufReader::new(file);
                for line in reader.lines() {
                    let line = line?;
                    let kv: MetadataKv = line.parse()?;
                    kvs.push(kv);
                }

                Ok(kvs)
            }
        }
    }

}

impl FromStr for MetadataKv {
    type Err = Error;
    fn from_str(value: &str) -> Result<Self> {
        let kvs:Vec<&str> = value.splitn(2, '=').collect();

        if kvs.len() != 2 {
            return Err(anyhow!("illegal kv: {}", value));
        }

        Ok(MetadataKv {
            key: kvs[0].to_owned(),
            value: kvs[1].to_owned(),
        })
    }
}

impl FromStr for MetadataDescriptor {
    type Err = Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        if s.starts_with('@') {
            Ok(MetadataDescriptor::File(PathBuf::from(&s[1..])))
        } else {
            Ok(MetadataDescriptor::Kv(s.parse()?))
        }
    }
}


#[derive(Debug, StructOpt)]
enum UpdateMetadata {
    Delete {
        key: String,
    },
    Put {
        metadata: Vec<MetadataDescriptor>,
    },
}

#[derive(Debug, StructOpt)]
enum UpdateVerb {
    Alias {
        alias: String,
    },
    Metadata(UpdateMetadata),
}

#[derive(Debug, StructOpt)]
struct Update {
    version: VersionIdentifier,
    #[structopt(subcommand)]
    verb: UpdateVerb,
}

#[derive(Debug, StructOpt)]
enum Command {
    Create {
        #[structopt(short,long)]
        alias: Option<String>,
        #[structopt(short,long)]
        metadata: Vec<MetadataDescriptor>,
        #[structopt(parse(from_os_str))]
        files: Vec<PathBuf>,
    },

    List {
        version: VersionIdentifier,
    },

    Metadata {
        version: VersionIdentifier,
    },

    Show {
        version: VersionIdentifier,
    },

    ListVersions {
        #[structopt(short, long)]
        num_versions: Option<usize>,
    },

    Update(Update),

    Extract {
        version: VersionIdentifier,

        #[structopt(short, long, parse(from_os_str))]
        prefix: Option<PathBuf>,
    },

    /// List versions of a specific path in the repo
    VersionsOf {
        /// List all versions of this path
        path: String,

        /// List only distinct hashes of this
        #[structopt(short, long)]
        distinct: bool,
    },

    /// List versions of a specific hash in the repo
    VersionsOfHash {
        /// List all versions of this hash
        hash: String,
    },

    ExtractFile {
        hash: String,
        #[structopt(short, long, parse(from_os_str))]
        output_path: Option<PathBuf>,
    },

    /// Command to import existing archives into a chunkr repo
    Import(import::Opts),
}

#[derive(Debug, StructOpt)]
struct Opts {
    #[structopt(short,long,parse(from_os_str))]
    repo_path: Option<PathBuf>,

    #[structopt(short, long)]
    verbose: bool,

    #[structopt(subcommand)]
    cmd: Command,
}

struct CliApi {
    repo: Repo,
    verbose: bool,
}

impl CliApi {
    fn open(repo_path: &PathBuf, init: bool, verbose: bool) -> Result<CliApi> {
        if !init {
            if !repo_path.exists() {
                return Err(anyhow!("repository path does not exist: {:?}", repo_path));
            }
        }

        let mut repo = Repo::open(repo_path)?;
        if init {
            repo.init()?;
        } else {
            repo.assert_initialized()?;
        }
        Ok(CliApi{repo, verbose})
    }

    fn extract_file_by_hash(&self, hash: String, output_path: Option<PathBuf>) -> Result<()> {
        let mut output = get_output_writer(output_path)?;
        self.repo.write_all(&hash, &mut output)?;
        Ok(())
    }

    fn add_file_to_version(verbose: bool, path: &PathBuf, builder: &mut VersionBuilder) -> Result<()> {
        if path.is_dir() {
            util::visit_dirs(&path, &mut move |de| {
                let hash = builder.store_file(&de.path())?; // TODO error handling
                if verbose {
                    println!("{} {}", &de.path().to_string_lossy(), &hash);
                }
                Ok(())
            })?;
        } else {
            let hash = builder.store_file(&path)?;
            if verbose {
                println!("{} {}", &path.to_string_lossy(), &hash);
            }
        }

        Ok(())
    }

    fn create_version(&mut self, alias: Option<String>, metadata: Vec<MetadataDescriptor>, files: Vec<PathBuf>) -> Result<()> {
        let mut meta = HashMap::new();

        for descriptor in metadata {
            for MetadataKv {key, value} in descriptor.to_vec()? {
                meta.insert(key, value);
            }
        }

        let mut builder = self.repo.add_version(alias.as_deref(), Some(&meta))?;

        let mut error = false;

        for file in files {
            if let Err(e) = Self::add_file_to_version(self.verbose, &file, &mut builder) {
                error = true;
                eprintln!("error importing {:?}: {}", file, e);
            }
        }

        let version_id = builder.finish()?;

        if self.verbose {
            println!("version {} {:?} {:?}", version_id, alias, &meta);
        }

        if error {
            Err(anyhow!("previous error when importing files"))
        } else {
            Ok(())
        }
    }

    fn list_versions(&mut self, num_versions: Option<usize>) -> Result<()> {
        for (n, version) in self.repo.get_versions()?.into_iter().enumerate() {
            if let Some(limit) = num_versions {
                if n >= limit {
                    break;
                }
            }
            let native_timestamp = NaiveDateTime::from_timestamp(version.timestamp, 0);
            println!("{} @ {}", version, native_timestamp);
            if self.verbose {
                let archive = self.repo.extract(&version.id);
                for (k,v) in &archive.get_metadata()? {
                    println!(" {} = {}", k, v);
                }
            }
        }
        Ok(())
    }

    fn list_files(&mut self, version: VersionIdentifier) -> Result<()> {
        let archive = self.repo.extract_by_identifier(&version)?;

        for file in archive.list_files_with_hash()? {
            if self.verbose {
                println!("{}", file);
            } else {
                println!("{}", file.path);
            }
        }

        Ok(())
    }

    fn extract_version(&mut self, version: VersionIdentifier, prefix: Option<PathBuf>) -> Result<()> {
        let archive = self.repo.extract_by_identifier(&version)?;

        for file in archive.list_files()? {
            if let Some(prefix) = &prefix {
                let mut path = prefix.clone();
                let file_path = PathBuf::from(&file);
                if file_path.has_root() {
                    let file_path = file_path.strip_prefix("/").unwrap();
                    path.push(file_path);
                } else {
                    path.push(file_path);
                }

                archive.write_file_by_path(&file, &path)?;
                if self.verbose {
                    println!("{} -> {}", file, path.to_string_lossy());
                }
            } else {
                archive.write_file(&file)?;
                if self.verbose {
                    println!("{}", file);
                }
            }
        }

        Ok(())
    }

    fn print_metadata(&mut self, version: VersionIdentifier) -> Result<()> {
        let archive = self.repo.extract_by_identifier(&version)?;
        println!("version {}", archive.version_id());
        if let Some(alias) = archive.get_alias()? {
            println!("alias {}", alias);
        }
        for (k,v) in &archive.get_metadata()? {
            println!(" {} = {}", k, v);
        }
        Ok(())
    }

    fn update_alias(&mut self, version: VersionIdentifier, alias: String) -> Result<()> {
        let archive = self.repo.extract_by_identifier(&version)?;
        archive.update_alias(&alias)?;
        Ok(())
    }

    fn update_metadata(&mut self, version: VersionIdentifier, metadata: UpdateMetadata) -> Result<()> {
        let mut archive = self.repo.extract_by_identifier(&version)?;
        match metadata {
            UpdateMetadata::Put {metadata} => {
                let mut meta = HashMap::new();
                for descriptor in metadata {
                    for MetadataKv {key, value} in descriptor.to_vec()? {
                        meta.insert(key, value);
                    }
                }

                archive.put_all_metadata(&meta)?;
            },
            UpdateMetadata::Delete {key} => {
                archive.delete_metadata(&key)?;
            },
        }
        Ok(())
    }

    fn print_versions_of_tree(&self, path: String, distinct: bool) -> Result<()> {
        let entries = if distinct {
            self.repo.get_all_hashes_for_tree(&path)
        } else {
            self.repo.get_version_info_for_tree(&path)
        };

        self.print_versions(entries?)
    }

    fn print_versions_of_hash(&self, hash: String) -> Result<()> {
        self.print_versions(self.repo.get_version_info_for_hash(&hash)?)
    }

    fn print_versions(&self, versions: Vec<TreeVersionInfo>) -> Result<()> {
        for entry in versions {

            let version_string = match entry.version_info.as_ref() {
                Some(version_info) => {
                    let timestamp = NaiveDateTime::from_timestamp(version_info.timestamp, 0);

                    format!(" @ {timestamp} {version_info:<15}",
                            version_info = format!("{}", version_info),
                            timestamp = timestamp,
                    )
                },
                None => String::new(),
            };

            let modified_time = entry.metadata.modified_ts
                .map(|f| NaiveDateTime::from_timestamp(f/1000, ((f%1000) * 1000000) as u32).to_string())
                .unwrap_or_default();

            println!("{path} {hash} {modified} {size}{version_string}",
                     version_string = version_string,
                     hash = entry.file_info.hash,
                     path = entry.file_info.path,
                     modified = modified_time,
                     size = entry.metadata.size,
            );
        }
        Ok(())
    }
}

fn get_output_writer(output_path: Option<PathBuf>) -> Result<Box<dyn Write>> {
    if let Some(path) = output_path {
        let file = std::fs::File::create(&path)?;
        Ok(Box::new(BufWriter::new(file)))
    } else {
        Ok(Box::new(std::io::stdout()))
    }
}

fn get_repo_path_from_env() -> Option<PathBuf> {
    std::env::var("CHUNKR_REPO").ok().map(String::into)
}

fn main() -> Result<()>{
    let opts: Opts = Opts::from_args();

    let repo_path = opts.repo_path.or_else(get_repo_path_from_env).unwrap_or("repo.db".into());

    if let Command::Import(import_opts) = opts.cmd {
        return import::import(&repo_path, import_opts, opts.verbose);
    }

    let init_repo = match &opts.cmd {
        Command::Create {..} => true,
        _ => false,
    };

    let mut cli_api = CliApi::open(&repo_path, init_repo, opts.verbose)?;

    let result = match opts.cmd {
        Command::ExtractFile{hash, output_path} => cli_api.extract_file_by_hash(hash, output_path),
        Command::Create{alias, metadata, files} => cli_api.create_version(alias, metadata, files),
        Command::ListVersions{num_versions} => cli_api.list_versions(num_versions),
        Command::Extract {version, prefix} => cli_api.extract_version(version, prefix),
        Command::Metadata {version} => cli_api.print_metadata(version),
        Command::Show {version} => cli_api.print_metadata(version),
        Command::List {version} => cli_api.list_files(version),
        Command::Update(Update {version, verb: UpdateVerb::Alias{alias}}) => cli_api.update_alias(version, alias),
        Command::Update(Update {version, verb: UpdateVerb::Metadata(metadata_update)}) => cli_api.update_metadata(version, metadata_update),
        Command::VersionsOf {path, distinct} => cli_api.print_versions_of_tree(path, distinct),
        Command::VersionsOfHash {hash} => cli_api.print_versions_of_hash(hash),
        unknown => Err(anyhow!("unknown cmd: {:?}", &unknown)),
    };

    if let Err(e) = result {
        println!("Error: {}", e);
    }

    Ok(())
}
